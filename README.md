# DS 721 Individual Project 4: Rust AWS Lambda and Step Functions

## Demo Video

![](Screen_Recording_2024-04-22_at_7.12.12_PM.mov)
## Description

This project is a simple data processing pipeline that orchestrates a Lambda function and Step Functions workflow. The pipeline includes two functions: one removes all special characters from the input text, and the other numbers the text. 

## Requirements

- Rust
- Cargo Lambda

## Set Up

1. **Create Rust Lambda Functions**:

   Create the Rust Lambda functions using Cargo Lambda:

   ```bash
   cargo lambda new remove_nonchar
   cargo lambda new remove_num
   ```

2. **Build and Deploy Functions to AWS Lambda**:

   Build and deploy the functions to AWS Lambda using Cargo Lambda:

   ```bash
   cargo build --release
   cargo lambda deploy --iam-role arn:aws:iam:yourAccountNumber:role/yourRole
   ```

   Replace `yourAccountNumber` and `yourRole` with your AWS IAM account number and role name respectively.

## Testing Locally

Test each Lambda function locally or on AWS Lambda to ensure they are working correctly.

---

![alt text](images/1.png)
![alt text](images/2.png)

### Deploying to AWS Lambda
![alt text](images/3.png)
![alt text](images/4.png)

### Working with Step Function
![alt text](images/6.png)
![alt text](images/7.png)
![alt text](images/5.png)

